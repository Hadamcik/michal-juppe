<?php

namespace App\Presenters;

use App\Components\CategoryMenu\ICategoryMenuFactory;
use App\Components\CategoryMenu\CategoryMenu;
use Nette\Application\UI\Presenter;

/**
 * Class BasePresenter
 * @package App\Presenters
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */
abstract class BasePresenter extends Presenter
{
    /** @var ICategoryMenuFactory @inject */
    public $menuFactory;

    /** @persistent */
    public $categoryId = null;

    /**
     * @param int $categoryId
     * @return string
     */
    public function getMenuLink($categoryId)
    {
        return $this->link('this', [
            'categoryId' => $categoryId
        ]);
    }

    /**
     * @return CategoryMenu
     */
    protected function createComponentMenu()
    {
        $component = $this->menuFactory->create();
        $component->getLink = [$this, 'getMenuLink'];
        return $component;
    }

    /**
     * @return int|null
     */
    protected function getCategoryId()
    {
        return $this->categoryId;
    }
}
