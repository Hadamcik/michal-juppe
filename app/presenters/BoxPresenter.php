<?php

namespace App\Presenters;

use App\Components\BoxForm\BoxForm;
use App\Components\BoxForm\IBoxFormFactory;
use App\Model\Box\BoxRepository;

/**
 * Class BoxPresenter
 * @package App\Presenters
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */
class BoxPresenter extends BasePresenter
{
    /** @var BoxRepository @inject */
    public $boxRepository;

    /** @var IBoxFormFactory @inject */
    public $boxFormFactory;

    /** @var int */
    private $id;

    /**
     * @return void
     */
    public function actionDefault()
    {
        $this->template->boxes = $this->boxRepository->findAll();
    }

    /**
     * @param int $id
     * @return void
     */
    public function actionEdit($id)
    {
        $this->setId($id);
    }

    /**
     * @return BoxForm
     */
    protected function createComponentEditBoxForm()
    {
        $component = $this->boxFormFactory->create();
        $component->setId($this->getId());

        return $component;
    }

    /**
     * @return BoxForm
     */
    protected function createComponentNewBoxForm()
    {
        return $this->boxFormFactory->create();
    }

    /**
     * @return int
     */
    private function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return BoxPresenter
     */
    private function setId($id)
    {
        $this->id = (int)$id;
        return $this;
    }
}
