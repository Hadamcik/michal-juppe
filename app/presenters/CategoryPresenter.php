<?php

namespace App\Presenters;

/**
 * Class CategoryPresenter
 * @package App\Presenters
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */
class CategoryPresenter extends BasePresenter
{
    /**
     * @param int $categoryId
     * @return string
     */
    public function getMenuLink($categoryId)
    {
        return $this->link('detail', [
            $categoryId
        ]);
    }

    /**
     * @return void
     */
    public function actionDefault()
    {
        $categoryId = $this->getCategoryId();
        if ($categoryId !== null) {

        } else {

        }
    }

    /**
     * @param int $id
     * @return void
     */
    public function actionDetail($id)
    {

    }
}
