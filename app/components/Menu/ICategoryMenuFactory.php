<?php

namespace App\Components\CategoryMenu;

/**
 * Interface ICategoryMenuFactory
 * @package App\Components\CategoryMenu
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */
interface ICategoryMenuFactory
{
    /**
     * @return CategoryMenu
     */
    public function create();
}
