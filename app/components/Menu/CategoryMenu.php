<?php

namespace App\Components\CategoryMenu;

use Nette\Application\UI\Control;

/**
 * Class CategoryMenu
 * @package App\Components\CategoryMenu
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */
class CategoryMenu extends Control
{
    /** @var callable */
    public $getLink;

    /**
     * @return void
     */
    public function render()
    {
        $this->template->getLink = $this->getLink;
        $this->template->categories = $this->getCategories();
        $this->template->setFile(__DIR__ . '/CategoryMenu.latte');
        $this->template->render();
    }

    /**
     * @return array
     */
    private function getCategories()
    {
        return [
            5 => 'Lodě',
            6 => 'Auta',
            7 => 'Něco'
        ];
    }
}
