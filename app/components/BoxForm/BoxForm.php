<?php

namespace App\Components\BoxForm;

use App\Model\Box\BoxRepository;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

/**
 * Class BoxForm
 * @package App\Components\BoxForm
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */
class BoxForm extends Control
{
    /** @var BoxRepository */
    private $boxRepository;

    /** @var int|null */
    private $id = null;

    /**
     * BoxForm constructor.
     * @param BoxRepository $boxRepository
     */
    public function __construct(BoxRepository $boxRepository)
    {
        parent::__construct();
        $this->boxRepository = $boxRepository;
    }

    /**
     * @param Form $form
     * @return void
     */
    public function processForm(Form $form)
    {
        $values = $form->getValues();

        $box = $this->boxRepository->get($this->getId());
        if ($box === false) {
            $this->boxRepository->insert($values->weight);
        } else {
            $this->boxRepository->update($this->getId(), $values->weight);
        }
    }

    /**
     * @return void
     */
    public function render()
    {
        $this->template->setFile(__DIR__ . '/BoxForm.latte');
        $this->template->render();
    }

    /**
     * @param int $id
     * @return BoxForm
     */
    public function setId($id)
    {
        $this->id = (int)$id;
        return $this;
    }

    /**
     * @return Form
     */
    protected function createComponentForm()
    {
        $form = new Form();

        $form->addText('weight', 'Váha');

        $form->addSubmit('submit', 'Odeslat');

        $box = $this->boxRepository->get($this->getId());
        if ($box !== false) {
            $form->setDefaults([
                'weight' => $box->weight
            ]);
        }

        $form->onSuccess[] = [$this, 'processForm'];

        return $form;
    }

    /**
     * @return int
     */
    private function getId()
    {
        return $this->id;
    }
}
