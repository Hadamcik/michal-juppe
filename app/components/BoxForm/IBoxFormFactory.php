<?php

namespace App\Components\BoxForm;

/**
 * Interface IBoxFormFactory
 * @package App\Components\BoxForm
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */
interface IBoxFormFactory
{
    /**
     * @return BoxForm
     */
    public function create();
}
