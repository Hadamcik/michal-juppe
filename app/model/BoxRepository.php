<?php

namespace App\Model\Box;

use App\Model\BaseRepository;

/**
 * Class BoxRepository
 * @package App\Model\Box
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */
class BoxRepository extends BaseRepository
{
    protected $tableName = 'box';

    /**
     * @return \Nette\Database\Table\IRow[]
     */
    public function findAll()
    {
        return $this->getTable()
            ->fetchAll();
    }

    /**
     * @param float $weight
     * @return void
     */
    public function insert($weight)
    {
        $this->getTable()
            ->insert([
                'weight' => $weight
            ]);
    }

    /**
     * @param int $id
     * @param float $weight
     * @return void
     */
    public function update($id, $weight)
    {
        $this->getTable()
            ->where('')
            ->update([
                'weight' => $weight
            ]);
    }
}
