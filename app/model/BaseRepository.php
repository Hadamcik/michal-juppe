<?php

namespace App\Model;

use Nette\Database\Context;
use Nette\Object;

/**
 * Class BaseRepository
 * @package App\Model
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */
class BaseRepository extends Object
{
    /** @var string */
    protected $tableName;

    /** @var Context */
    private $database;

    /**
     * BaseRepository constructor.
     * @param Context $database
     */
    public function __construct(Context $database)
    {
        $this->database = $database;
    }

    /**
     * @param int $id
     * @return \Nette\Database\Table\IRow|false
     */
    public function get($id)
    {
        return $this->getTable()
            ->get($id);
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    protected function getTable()
    {
        return $this->database->table($this->getTableName());
    }

    /**
     * @return string
     */
    private function getTableName()
    {
        return $this->tableName;
    }
}
